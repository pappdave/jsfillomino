
function removeAllEventListeners(element) {
    var newElement = element.cloneNode(true);
    element.parentNode.replaceChild(newElement, element);
}


var Game = Game || {};

Game.State = Game.State || {};

Game.State.Level = function(selectedLevel) {
    this.canvas = document.getElementById('gameCanvas');
    this.selectedLevel = selectedLevel;
};

Game.State.Level.prototype.start = function() {
    document.getElementById('level-select').style.display = 'none';
    this.canvas.style.display = 'inline'
    var board = new Fillomino.Board(this.selectedLevel.initialState);
    var display = new Fillomino.CanvasDisplay(this.canvas, board);

    this.canvas.addEventListener('mouseup', function(event) { display.handleMouseUp(event); });
    this.canvas.addEventListener('mousedown', function(event) { display.handleMouseDown(event); });
    this.canvas.addEventListener('mousemove', function(event) { display.handleMouseMove(event); });
    this.canvas.addEventListener('contextmenu', function(event) { display.handleRightClick(event); event.preventDefault(); });

    var displayInterval = setInterval(function() { display.draw(); }, 1000 / 60);

    var that = this;
    board.winHook = function() {
        clearInterval(displayInterval);
        that.canvas.style.display = 'none'
        removeAllEventListeners(that.canvas);
        console.log("YOU HAVE WON!!!");
        Game.currentState = new Game.State.LevelSelect();
    }
}

Game.State.LevelSelect = function() {
    
    function removeAllChildNodes(element) {
        while (element.lastChild) {
            element.removeChild(element.lastChild);
        }
    }

    function startLevel(level) {
        Game.currentState = new Game.State.Level(level);
        Game.currentState.start();
    }
    
    levelSelectDiv = document.getElementById('level-select');
    levelSelectDiv.style.display = 'inline';

    for (var i = 0; i < levels.length; ++i) {
        console.log("Adding button");
        var button = document.createElement('button');
        button.className = "levelSelectButton"
        var text = document.createTextNode(levels[i].name);
        button.appendChild(text);
        
        (function (level) {
            button.addEventListener('click', function() { 
                removeAllChildNodes(levelSelectDiv);
                startLevel(level)
            });
        }(levels[i]));

        levelSelectDiv.appendChild(button);
    }
}


Game.currentState = new Game.State.LevelSelect();
