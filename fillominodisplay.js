
var Fillomino = Fillomino || {};

Fillomino.ConsoleDisplay = {
    draw: function(board) {
        console.log(board);
        for (var i = 0; i < board.currentState.length; ++i) {
            console.log(board.currentState[i]);
        }
    }
}


Fillomino.CanvasDisplay = function(canvas, board) {
    this.canvas = canvas;
    this.board = board;
 
    this.topLeftOffsetRatio = [0.1, 0.1];
    this.bottomRightOffsetRatio = [0.1, 0.2];
    
    this.boardTopLeft = [
        0 + canvas.width * this.topLeftOffsetRatio[0],
        0 + canvas.height * this.topLeftOffsetRatio[1]
    ];
    
    this.boardBottomRight = [
        canvas.width - canvas.width * this.bottomRightOffsetRatio[0],
        canvas.height - canvas.height * this.bottomRightOffsetRatio[1]
    ];

    this.horizontalCellSize = (this.boardBottomRight[0] - this.boardTopLeft[0]) / this.board.currentState[0].length;
    this.verticalCellSize = (this.boardBottomRight[1] - this.boardTopLeft[1]) / this.board.currentState.length;
    
    this.valueToColorMap = {
        0: 'white',
        1: 'red',
        2: 'purple',
        3: 'fuchsia',
        4: 'green',
        5: 'lime',
        6: 'olive',
        7: 'yellow',
        8: 'navy',
        9: 'blue',
        10: 'teal',
        11: 'aqua'
    }
}

Fillomino.CanvasDisplay.prototype.draw = function() {
    var ctx = this.canvas.getContext('2d');

    ctx.fillStyle = '#EEEEEE';
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    
    ctx.fillStyle = '#DDDDDD';
    ctx.fillRect(
        this.boardTopLeft[0],
        this.boardTopLeft[1],
        this.boardBottomRight[0] - this.boardTopLeft[0],
        this.boardBottomRight[1] - this.boardTopLeft[1]
    );
    
    ctx.font = '14px Arial';
    ctx.textBaseline = 'middle';
    ctx.textAling = 'center';
    ctx.strokeStyle = '#999999';
    for (var i = 0; i < this.board.currentState[0].length; ++i) {
        for (var j = 0; j < this.board.currentState.length; ++j) {
            ctx.fillStyle = this.valueToColorMap[this.board.getCell([j, i])];
            ctx.fillRect(
                this.boardTopLeft[0] + i * this.horizontalCellSize,
                this.boardTopLeft[1] + j * this.verticalCellSize,
                this.horizontalCellSize,
                this.verticalCellSize
            );
            
            ctx.strokeRect(
                this.boardTopLeft[0] + i * this.horizontalCellSize,
                this.boardTopLeft[1] + j * this.verticalCellSize,
                this.horizontalCellSize,
                this.verticalCellSize
            );
            
            ctx.fillStyle = '#FFFFFF';
            ctx.fillText(
                this.board.getCell([j, i]),
                this.boardTopLeft[0] + (i + 0.5) * this.horizontalCellSize,
                this.boardTopLeft[1] + (j + 0.5) * this.verticalCellSize
            );
        }
    }
}

Fillomino.CanvasDisplay.prototype.getCellByPageCoordinate = function(pageX, pageY) {
    var x = pageX - this.canvas.offsetLeft - this.boardTopLeft[0];
    var y = pageY - this.canvas.offsetTop - this.boardTopLeft[1];
    
    var cellX = Math.floor(x / this.horizontalCellSize);
    var cellY = Math.floor(y / this.verticalCellSize);
    
    return [cellY, cellX];
}

Fillomino.CanvasDisplay.prototype.handleMouseDown = function(event) {
    this.dragging = true;
    this.selectedCell = this.getCellByPageCoordinate(event.pageX, event.pageY);
}

Fillomino.CanvasDisplay.prototype.handleMouseUp = function(event) {
    this.dragging = false;
}

Fillomino.CanvasDisplay.prototype.handleMouseMove = function(event) {
    if (this.dragging) {
        var currentCell = this.getCellByPageCoordinate(event.pageX, event.pageY);
        this.board.connect(this.selectedCell, currentCell);
    }
}

Fillomino.CanvasDisplay.prototype.handleRightClick = function(event) {
    currentCell = this.getCellByPageCoordinate(event.pageX, event.pageY);
    this.board.remove(currentCell);
}