
// for the rules of fillomino, see https://en.wikipedia.org/wiki/Fillomino

var Fillomino = Fillomino || {};

/*
initialBoard is a matrix of the initial board state, where 0 means an empty
cell. For example:
[
    [3, 0, 0, 6, 6],
    [5, 0, 4, 0, 6],
    [0, 5, 4, 0, 6],
    [0, 1, 0, 6, 0],
    [0, 0, 3, 1, 2]
]
*/

function cloneMatrix(matrix) {
    var clone = [];
    for (var i = 0; i < matrix.length; ++i) {
        clone.push(matrix[i].slice());
    }
    return clone;
}

Fillomino.Board = function(initialState) {
    this.initialState = cloneMatrix(initialState);
    this.currentState = cloneMatrix(initialState);
};

Fillomino.Board.prototype.getCell = function(coord) {
    return this.currentState[coord[0]][coord[1]];
}

Fillomino.Board.prototype.setCell = function(coord, value) {
    this.currentState[coord[0]][coord[1]] = value;
}

Fillomino.Board.prototype.isEmpty = function(coord) {
    return this.getCell(coord) === 0;
}

Fillomino.Board.prototype.didExistInitially = function(coord) {
    return this.initialState[coord[0]][coord[1]] !== 0;
}

Fillomino.Board.prototype.connect = function(from, to) {
    if (this.isEmpty(from) || this.didExistInitially(to)) {
        return;
    }
    
    this.setCell(to, this.getCell(from));
    
    if (this.isCompleted() && this.hasOwnProperty('winHook')) {
        this.winHook();
    }
}

Fillomino.Board.prototype.remove = function(coord) {
    if (!this.didExistInitially(coord)) {
        this.setCell(coord, 0);
    }
}

Fillomino.Board.prototype.getConnectedCellCount = function(i, j, visited) {
    var count = 1;
    visited[i][j] = true;
    
    var offsets = [[0, 1], [1, 0], [0, -1], [-1, 0]];
    var that = this;
    offsets.forEach(function(offset) {
        var newI = i + offset[0];
        var newJ = j + offset[1];

        if (newI < 0 || newI >= visited.length) {
            return;
        }
        
        if (newJ < 0 || newJ >= visited[newI].length) {
            return;
        }
        
        if (!visited[newI][newJ]) {
            if (that.currentState[i][j] == that.currentState[newI][newJ]) {
                count += that.getConnectedCellCount(newI, newJ, visited);
            }
        }
    });
    
    return count;
}

Fillomino.Board.prototype.isCompleted = function() {
    var visited = [];
    for (var i = 0; i < this.currentState.length; ++i) {
        visited[i] = [];
        for (var j = 0; j < this.currentState[i].length; ++j) {
            visited[i][j] = false;
        }
    }
    
    for (var i = 0; i < this.currentState.length; ++i) {
        for (var j = 0; j < this.currentState[i].length; ++j) {
            if (!visited[i][j]) {
                connectedCellCount = this.getConnectedCellCount(i, j, visited);

                if (connectedCellCount != this.currentState[i][j]) {
                    return false;
                }
            }
        }
    }
    
    return true;
}
